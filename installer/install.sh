#!/bin/sh
if [ -n "$JAVA_HOME" ] ; then
  SCRIPT_PATH=`dirname $0`
  chmod +x "${SCRIPT_PATH}/gradlew"
  version=$($JAVA_HOME/bin/java -version 2>&1 | awk -F '"' '/version/ {print $2}')
else
  echo "Error: JAVA_HOME is not set, please set it in your environment."
  exit 255
fi

EXPECTED_JAVA_MAJOR_VER=11

if [ -n "$version" ] ; then
  if [ "$version" \< "${EXPECTED_JAVA_MAJOR_VER}" ]; then
    echo "Wrong java version is set - "$version", Installer requires at least java ${EXPECTED_JAVA_MAJOR_VER}"
    exit 255
  fi
fi
export INSTALLER_WORKING_DIR="${SCRIPT_PATH}"

java -classpath "${SCRIPT_PATH}/libs/installer-cli-21.05.0-RC41.jar:${SCRIPT_PATH}/libs/commons-lang-2.6.jar:${SCRIPT_PATH}/libs/groovy-test-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-ant-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-astbuilder-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-cli-picocli-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-groovysh-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-console-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-datetime-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-groovydoc-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-docgenerator-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-jmx-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-json-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-jsr223-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-macro-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-nio-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-servlet-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-sql-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-swing-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-templates-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-test-junit5-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-xml-3.0.8.jar:${SCRIPT_PATH}/libs/groovy-3.0.8.jar:${SCRIPT_PATH}/libs/commons-cli-1.2.jar:" de.hybris.installer.CmdHandler "$@"