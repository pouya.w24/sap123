/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21.10.2022, 11:34:57                        ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.sap.productconfig.b2bfrontend.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedSapproductconfigb2baddonConstants
{
	public static final String EXTENSIONNAME = "sapproductconfigb2baddon";
	public static class TC
	{
		public static final String CPQREORDERACTION = "CPQReorderAction".intern();
	}
	
	protected GeneratedSapproductconfigb2baddonConstants()
	{
		// private constructor
	}
	
	
}
