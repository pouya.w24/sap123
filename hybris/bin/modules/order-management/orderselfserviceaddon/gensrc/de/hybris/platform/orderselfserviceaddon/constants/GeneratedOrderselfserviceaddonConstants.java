/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21.10.2022, 11:34:57                        ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.orderselfserviceaddon.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedOrderselfserviceaddonConstants
{
	public static final String EXTENSIONNAME = "orderselfserviceaddon";
	public static class TC
	{
		public static final String CANCELORDERACTION = "CancelOrderAction".intern();
		public static final String CANCELRETURNACTION = "CancelReturnAction".intern();
		public static final String RETURNORDERACTION = "ReturnOrderAction".intern();
	}
	
	protected GeneratedOrderselfserviceaddonConstants()
	{
		// private constructor
	}
	
	
}
