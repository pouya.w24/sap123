/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21.10.2022, 11:34:57                        ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.multicountrysampledataaddon.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedMulticountrysampledataaddonConstants
{
	public static final String EXTENSIONNAME = "multicountrysampledataaddon";
	
	protected GeneratedMulticountrysampledataaddonConstants()
	{
		// private constructor
	}
	
	
}
