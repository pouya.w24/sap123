/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21.10.2022, 11:34:57                        ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.security.captcha.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedCaptchaaddonConstants
{
	public static final String EXTENSIONNAME = "captchaaddon";
	public static class Attributes
	{
		public static class BaseStore
		{
			public static final String CAPTCHACHECKENABLED = "captchaCheckEnabled".intern();
		}
	}
	
	protected GeneratedCaptchaaddonConstants()
	{
		// private constructor
	}
	
	
}
