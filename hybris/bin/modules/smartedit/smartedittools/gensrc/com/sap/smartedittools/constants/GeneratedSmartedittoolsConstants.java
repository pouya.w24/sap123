/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21.10.2022, 11:34:57                        ---
 * ----------------------------------------------------------------
 */
package com.sap.smartedittools.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedSmartedittoolsConstants
{
	public static final String EXTENSIONNAME = "smartedittools";
	
	protected GeneratedSmartedittoolsConstants()
	{
		// private constructor
	}
	
	
}
