/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21.10.2022, 11:34:57                        ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.secureportaladdon.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedSecureportaladdonConstants
{
	public static final String EXTENSIONNAME = "secureportaladdon";
	public static class TC
	{
		public static final String B2BREGISTRATION = "B2BRegistration".intern();
		public static final String B2BREGISTRATIONAPPROVEDPROCESS = "B2BRegistrationApprovedProcess".intern();
		public static final String B2BREGISTRATIONPROCESS = "B2BRegistrationProcess".intern();
		public static final String B2BREGISTRATIONREJECTEDPROCESS = "B2BRegistrationRejectedProcess".intern();
		public static final String CMSSECUREPORTALRESTRICTION = "CMSSecurePortalRestriction".intern();
	}
	public static class Attributes
	{
		public static class CMSSite
		{
			public static final String ENABLEREGISTRATION = "enableRegistration".intern();
			public static final String REQUIRESAUTHENTICATION = "requiresAuthentication".intern();
		}
	}
	
	protected GeneratedSecureportaladdonConstants()
	{
		// private constructor
	}
	
	
}
