/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21.10.2022, 11:34:57                        ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercewebservicescommons.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedCommercewebservicescommonsConstants
{
	public static final String EXTENSIONNAME = "commercewebservicescommons";
	public static class TC
	{
		public static final String OLDCARTREMOVALCRONJOB = "OldCartRemovalCronJob".intern();
		public static final String OLDPAYMENTSUBSCRIPTIONRESULTREMOVALCRONJOB = "OldPaymentSubscriptionResultRemovalCronJob".intern();
		public static final String ORDERSTATUSUPDATECLEANERCRONJOB = "OrderStatusUpdateCleanerCronJob".intern();
		public static final String PAYMENTSUBSCRIPTIONRESULT = "PaymentSubscriptionResult".intern();
		public static final String PRODUCTEXPRESSUPDATECLEANERCRONJOB = "ProductExpressUpdateCleanerCronJob".intern();
	}
	public static class Attributes
	{
		// no constants defined.
	}
	
	protected GeneratedCommercewebservicescommonsConstants()
	{
		// private constructor
	}
	
	
}
