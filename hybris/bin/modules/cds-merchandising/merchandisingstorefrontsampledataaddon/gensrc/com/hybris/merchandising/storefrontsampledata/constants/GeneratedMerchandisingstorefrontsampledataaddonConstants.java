/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21.10.2022, 11:34:57                        ---
 * ----------------------------------------------------------------
 */
package com.hybris.merchandising.storefrontsampledata.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedMerchandisingstorefrontsampledataaddonConstants
{
	public static final String EXTENSIONNAME = "merchandisingstorefrontsampledataaddon";
	
	protected GeneratedMerchandisingstorefrontsampledataaddonConstants()
	{
		// private constructor
	}
	
	
}
