/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21.10.2022, 11:34:57                        ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.personalizationyprofile.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedPersonalizationyprofileConstants
{
	public static final String EXTENSIONNAME = "personalizationyprofile";
	public static class Attributes
	{
		public static class CxConfig
		{
			public static final String ORDERMAPPERSEGMENTMAP = "orderMapperSegmentMap".intern();
		}
		public static class CxMapperScript
		{
			public static final String REQUIREDFIELDS = "requiredFields".intern();
		}
	}
	
	protected GeneratedPersonalizationyprofileConstants()
	{
		// private constructor
	}
	
	
}
