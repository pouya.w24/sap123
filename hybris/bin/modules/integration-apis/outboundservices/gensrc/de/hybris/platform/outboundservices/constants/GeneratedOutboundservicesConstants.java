/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21.10.2022, 11:34:57                        ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.outboundservices.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedOutboundservicesConstants
{
	public static final String EXTENSIONNAME = "outboundservices";
	public static class TC
	{
		public static final String OUTBOUNDREQUEST = "OutboundRequest".intern();
		public static final String OUTBOUNDREQUESTMEDIA = "OutboundRequestMedia".intern();
		public static final String OUTBOUNDSOURCE = "OutboundSource".intern();
	}
	public static class Attributes
	{
		// no constants defined.
	}
	public static class Enumerations
	{
		public static class OutboundSource
		{
			public static final String UNKNOWN = "UNKNOWN".intern();
			public static final String OUTBOUNDSYNC = "OUTBOUNDSYNC".intern();
			public static final String WEBHOOKSERVICES = "WEBHOOKSERVICES".intern();
		}
	}
	
	protected GeneratedOutboundservicesConstants()
	{
		// private constructor
	}
	
	
}
