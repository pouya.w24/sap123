/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21.10.2022, 11:34:57                        ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.webhookservices.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedWebhookservicesConstants
{
	public static final String EXTENSIONNAME = "webhookservices";
	public static class TC
	{
		public static final String WEBHOOKCONFIGURATION = "WebhookConfiguration".intern();
	}
	public static class Attributes
	{
		// no constants defined.
	}
	public static class Enumerations
	{
		public static class DestinationChannel
		{
			public static final String WEBHOOKSERVICES = "WEBHOOKSERVICES".intern();
		}
	}
	
	protected GeneratedWebhookservicesConstants()
	{
		// private constructor
	}
	
	
}
