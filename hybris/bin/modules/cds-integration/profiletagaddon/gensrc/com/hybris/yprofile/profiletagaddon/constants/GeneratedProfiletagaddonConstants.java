/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21.10.2022, 11:34:57                        ---
 * ----------------------------------------------------------------
 */
package com.hybris.yprofile.profiletagaddon.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedProfiletagaddonConstants
{
	public static final String EXTENSIONNAME = "profiletagaddon";
	public static class TC
	{
		public static final String PROFILETAGSCRIPTCOMPONENT = "ProfileTagScriptComponent".intern();
	}
	
	protected GeneratedProfiletagaddonConstants()
	{
		// private constructor
	}
	
	
}
