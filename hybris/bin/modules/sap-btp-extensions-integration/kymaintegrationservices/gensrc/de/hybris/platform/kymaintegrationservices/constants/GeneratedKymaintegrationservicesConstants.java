/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21.10.2022, 11:34:57                        ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.kymaintegrationservices.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedKymaintegrationservicesConstants
{
	public static final String EXTENSIONNAME = "kymaintegrationservices";
	public static class Enumerations
	{
		public static class DestinationChannel
		{
			public static final String KYMA = "KYMA".intern();
		}
	}
	
	protected GeneratedKymaintegrationservicesConstants()
	{
		// private constructor
	}
	
	
}
