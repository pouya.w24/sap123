/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at 21.10.2022, 11:34:57                        ---
 * ----------------------------------------------------------------
 *  
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.searchprovidercssearchservices.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated(since = "ages", forRemoval = false)
@SuppressWarnings({"unused","cast"})
public class GeneratedSearchprovidercssearchservicesConstants
{
	public static final String EXTENSIONNAME = "searchprovidercssearchservices";
	public static class TC
	{
		public static final String CSSEARCHSNSEARCHPROVIDERCONFIGURATION = "CSSearchSnSearchProviderConfiguration".intern();
	}
	public static class Attributes
	{
		// no constants defined.
	}
	
	protected GeneratedSearchprovidercssearchservicesConstants()
	{
		// private constructor
	}
	
	
}
