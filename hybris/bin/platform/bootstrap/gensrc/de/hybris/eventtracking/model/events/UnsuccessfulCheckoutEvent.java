/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:01
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.eventtracking.model.events;

import java.io.Serializable;

import de.hybris.eventtracking.model.events.AbstractTrackingEvent;

public  class UnsuccessfulCheckoutEvent extends AbstractTrackingEvent 
{

	
	public UnsuccessfulCheckoutEvent()
	{
		super();
	}

	public UnsuccessfulCheckoutEvent(final Serializable source)
	{
		super(source);
	}
	


}
