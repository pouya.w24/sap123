/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:02
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.solrfacetsearch.config;

public enum WildcardType
{

	/** <i>Generated enum value</i> for <code>WildcardType.PREFIX</code> value defined at extension <code>solrfacetsearch</code>. */
	PREFIX , 

	/** <i>Generated enum value</i> for <code>WildcardType.POSTFIX</code> value defined at extension <code>solrfacetsearch</code>. */
	POSTFIX , 

	/** <i>Generated enum value</i> for <code>WildcardType.PREFIX_AND_POSTFIX</code> value defined at extension <code>solrfacetsearch</code>. */
	PREFIX_AND_POSTFIX  


}
