/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:00
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.acceleratorfacades.cart.action;

/**
 * Accelerator cart entry actions
 */
public enum CartEntryAction
{

	/** <i>Generated enum value</i> for <code>CartEntryAction.REMOVE</code> value defined at extension <code>acceleratorfacades</code>. */
	REMOVE , 

	/** <i>Generated enum value</i> for <code>CartEntryAction.SAVEFORLATER</code> value defined at extension <code>selectivecartsplitlistaddon</code>. */
	SAVEFORLATER , 

	/** <i>Generated enum value</i> for <code>CartEntryAction.CPQ_COPY</code> value defined at extension <code>ysapproductconfigaddon</code>. */
	CPQ_COPY  


}
