/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:03
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.entitlementfacades.enums;

public enum EntitlementStatus
{

	/** <i>Generated enum value</i> for <code>EntitlementStatus.ACTIVE</code> value defined at extension <code>entitlementfacades</code>. */
	ACTIVE , 

	/** <i>Generated enum value</i> for <code>EntitlementStatus.SUSPENDED</code> value defined at extension <code>entitlementfacades</code>. */
	SUSPENDED , 

	/** <i>Generated enum value</i> for <code>EntitlementStatus.REVOKED</code> value defined at extension <code>entitlementfacades</code>. */
	REVOKED  


}
