/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:03
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.solrfacetsearch.config;

public enum QueryMethod
{

	/** <i>Generated enum value</i> for <code>QueryMethod.GET</code> value defined at extension <code>solrfacetsearch</code>. */
	GET , 

	/** <i>Generated enum value</i> for <code>QueryMethod.POST</code> value defined at extension <code>solrfacetsearch</code>. */
	POST  


}
