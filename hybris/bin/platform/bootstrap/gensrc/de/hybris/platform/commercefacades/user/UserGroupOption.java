/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:01
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercefacades.user;

public enum UserGroupOption
{

	/** <i>Generated enum value</i> for <code>UserGroupOption.BASIC</code> value defined at extension <code>commercefacades</code>. */
	BASIC , 

	/** <i>Generated enum value</i> for <code>UserGroupOption.MEMBERS</code> value defined at extension <code>commercefacades</code>. */
	MEMBERS , 

	/** <i>Generated enum value</i> for <code>UserGroupOption.SUBGROUPS</code> value defined at extension <code>commercefacades</code>. */
	SUBGROUPS  


}
