/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:00
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercefacades.promotion;

public enum PromotionOption
{

	/** <i>Generated enum value</i> for <code>PromotionOption.BASIC</code> value defined at extension <code>commercefacades</code>. */
	BASIC , 

	/** <i>Generated enum value</i> for <code>PromotionOption.EXTENDED</code> value defined at extension <code>commercefacades</code>. */
	EXTENDED  


}
