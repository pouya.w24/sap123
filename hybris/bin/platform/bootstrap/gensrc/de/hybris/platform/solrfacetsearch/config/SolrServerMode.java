/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:34:59
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.solrfacetsearch.config;

public enum SolrServerMode
{

	/** <i>Generated enum value</i> for <code>SolrServerMode.STANDALONE</code> value defined at extension <code>solrfacetsearch</code>. */
	STANDALONE , 

	/** <i>Generated enum value</i> for <code>SolrServerMode.EMBEDDED</code> value defined at extension <code>solrfacetsearch</code>. */
	EMBEDDED , 

	/** <i>Generated enum value</i> for <code>SolrServerMode.CLOUD</code> value defined at extension <code>solrfacetsearch</code>. */
	CLOUD , 

	/** <i>Generated enum value</i> for <code>SolrServerMode.XML_EXPORT</code> value defined at extension <code>solrfacetsearch</code>. */
	XML_EXPORT  


}
