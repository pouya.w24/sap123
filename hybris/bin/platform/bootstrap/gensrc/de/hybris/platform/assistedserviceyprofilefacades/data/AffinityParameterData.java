/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:34:59
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.assistedserviceyprofilefacades.data;

import java.io.Serializable;


import java.util.Objects;
public  class AffinityParameterData  implements Serializable 

{

	/** Default serialVersionUID value. */
 
	private static final long serialVersionUID = 1L;

	/** <i>Generated property</i> for <code>AffinityParameterData.sizeLimit</code> property defined at extension <code>assistedserviceyprofilefacades</code>. */
	
	private int sizeLimit;
	
	public AffinityParameterData()
	{
		// default constructor
	}
	
	public void setSizeLimit(final int sizeLimit)
	{
		this.sizeLimit = sizeLimit;
	}

	public int getSizeLimit() 
	{
		return sizeLimit;
	}
	

}