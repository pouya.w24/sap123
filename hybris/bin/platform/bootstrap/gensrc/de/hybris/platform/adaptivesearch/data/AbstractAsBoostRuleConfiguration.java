/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:02
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.adaptivesearch.data;

import de.hybris.platform.adaptivesearch.data.AbstractAsItemConfiguration;


import java.util.Objects;
public abstract  class AbstractAsBoostRuleConfiguration extends AbstractAsItemConfiguration 

{


	
	public AbstractAsBoostRuleConfiguration()
	{
		// default constructor
	}
	

}