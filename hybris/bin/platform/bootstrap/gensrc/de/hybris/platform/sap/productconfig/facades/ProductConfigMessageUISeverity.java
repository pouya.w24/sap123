/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:04
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.sap.productconfig.facades;

public enum ProductConfigMessageUISeverity
{

	/** <i>Generated enum value</i> for <code>ProductConfigMessageUISeverity.CONFIG</code> value defined at extension <code>sapproductconfigfacades</code>. */
	CONFIG , 

	/** <i>Generated enum value</i> for <code>ProductConfigMessageUISeverity.INFO</code> value defined at extension <code>sapproductconfigfacades</code>. */
	INFO , 

	/** <i>Generated enum value</i> for <code>ProductConfigMessageUISeverity.ERROR</code> value defined at extension <code>sapproductconfigfacades</code>. */
	ERROR  


}
