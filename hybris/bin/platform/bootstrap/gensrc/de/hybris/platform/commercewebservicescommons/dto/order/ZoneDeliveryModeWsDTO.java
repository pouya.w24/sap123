/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:01
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercewebservicescommons.dto.order;

import de.hybris.platform.commercewebservicescommons.dto.order.DeliveryModeWsDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import java.util.Objects;
/**
 * Representation of a Zone Delivery Mode
 */
@ApiModel(value="ZoneDeliveryMode", description="Representation of a Zone Delivery Mode")
public  class ZoneDeliveryModeWsDTO extends DeliveryModeWsDTO 

{


	
	public ZoneDeliveryModeWsDTO()
	{
		// default constructor
	}
	

}