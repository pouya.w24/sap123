/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:03
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercefacades.product.data;

public enum PriceDataType
{

	/** <i>Generated enum value</i> for <code>PriceDataType.BUY</code> value defined at extension <code>commercefacades</code>. */
	BUY , 

	/** <i>Generated enum value</i> for <code>PriceDataType.FROM</code> value defined at extension <code>commercefacades</code>. */
	FROM  


}
