/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:04
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsfacades.page;

public enum DisplayCondition
{

	/** <i>Generated enum value</i> for <code>DisplayCondition.PRIMARY</code> value defined at extension <code>cmsfacades</code>. */
	PRIMARY , 

	/** <i>Generated enum value</i> for <code>DisplayCondition.VARIATION</code> value defined at extension <code>cmsfacades</code>. */
	VARIATION  


}
