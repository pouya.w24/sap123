/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:03
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.subscriptionfacades.action;

public enum SubscriptionUpdateActionEnum
{

	/** <i>Generated enum value</i> for <code>SubscriptionUpdateActionEnum.CANCEL</code> value defined at extension <code>subscriptionfacades</code>. */
	CANCEL  


}
