/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:04
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.personalizationservices.enums;

public enum CxActionType
{

	/** <i>Generated enum value</i> for <code>CxActionType.CxAbstractAction</code> value defined at extension <code>personalizationservices</code>. */
	CXABSTRACTACTION , 

	/** <i>Generated enum value</i> for <code>CxActionType.CxCmsAction</code> value defined at extension <code>personalizationcms</code>. */
	CXCMSACTION  


}
