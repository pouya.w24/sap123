/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:01
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.payment.dto;

public enum TransactionStatus
{

	/** <i>Generated enum value</i> for <code>TransactionStatus.ACCEPTED</code> value defined at extension <code>payment</code>. */
	ACCEPTED , 

	/** <i>Generated enum value</i> for <code>TransactionStatus.ERROR</code> value defined at extension <code>payment</code>. */
	ERROR , 

	/** <i>Generated enum value</i> for <code>TransactionStatus.REJECTED</code> value defined at extension <code>payment</code>. */
	REJECTED , 

	/** <i>Generated enum value</i> for <code>TransactionStatus.REVIEW</code> value defined at extension <code>payment</code>. */
	REVIEW  


}
