/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:03
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.store.pojo;

public enum StoreCountType
{

	/** <i>Generated enum value</i> for <code>StoreCountType.COUNTRY</code> value defined at extension <code>basecommerce</code>. */
	COUNTRY , 

	/** <i>Generated enum value</i> for <code>StoreCountType.REGION</code> value defined at extension <code>basecommerce</code>. */
	REGION  


}
