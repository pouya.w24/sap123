/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:34:59
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.personalizationpromotions.rao;

import java.io.Serializable;


import java.util.Objects;
public  class CxPromotionActionResultRAO  implements Serializable 

{

	/** Default serialVersionUID value. */
 
	private static final long serialVersionUID = 1L;

	/** <i>Generated property</i> for <code>CxPromotionActionResultRAO.promotionId</code> property defined at extension <code>personalizationpromotions</code>. */
	
	private String promotionId;
	
	public CxPromotionActionResultRAO()
	{
		// default constructor
	}
	
	public void setPromotionId(final String promotionId)
	{
		this.promotionId = promotionId;
	}

	public String getPromotionId() 
	{
		return promotionId;
	}
	

}