/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:34:59
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.searchprovidercssearchservices.search.data;

public enum MatchTypeDTO
{

	/** <i>Generated enum value</i> for <code>MatchTypeDTO.ANY</code> value defined at extension <code>searchprovidercssearchservices</code>. */
	ANY , 

	/** <i>Generated enum value</i> for <code>MatchTypeDTO.ALL</code> value defined at extension <code>searchprovidercssearchservices</code>. */
	ALL  


}
