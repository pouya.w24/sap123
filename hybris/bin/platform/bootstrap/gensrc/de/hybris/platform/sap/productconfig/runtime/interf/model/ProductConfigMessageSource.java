/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:04
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.sap.productconfig.runtime.interf.model;

public enum ProductConfigMessageSource
{

	/** <i>Generated enum value</i> for <code>ProductConfigMessageSource.ENGINE</code> value defined at extension <code>sapproductconfigruntimeinterface</code>. */
	ENGINE , 

	/** <i>Generated enum value</i> for <code>ProductConfigMessageSource.RULE</code> value defined at extension <code>sapproductconfigrules</code>. */
	RULE  


}
