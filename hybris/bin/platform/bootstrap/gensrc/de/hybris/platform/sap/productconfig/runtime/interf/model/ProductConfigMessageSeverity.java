/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:34:59
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.sap.productconfig.runtime.interf.model;

public enum ProductConfigMessageSeverity
{

	/** <i>Generated enum value</i> for <code>ProductConfigMessageSeverity.ERROR</code> value defined at extension <code>sapproductconfigruntimeinterface</code>. */
	ERROR , 

	/** <i>Generated enum value</i> for <code>ProductConfigMessageSeverity.WARNING</code> value defined at extension <code>sapproductconfigruntimeinterface</code>. */
	WARNING , 

	/** <i>Generated enum value</i> for <code>ProductConfigMessageSeverity.INFO</code> value defined at extension <code>sapproductconfigruntimeinterface</code>. */
	INFO  


}
