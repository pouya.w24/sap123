/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:00
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmssmarteditwebservices.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import java.util.Objects;
/**
 * @deprecated no longer needed
 */
@ApiModel(value="CategoryPageWsDTO")
@Deprecated(since = "1811", forRemoval = true)
public  class CategoryPageWsDTO extends AbstractPageWsDTO 

{


	
	public CategoryPageWsDTO()
	{
		// default constructor
	}
	

}