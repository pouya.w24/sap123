/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:02
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsfacades.data;


import java.util.Objects;
/**
 * @deprecated no longer needed
 */
@Deprecated(since = "6.6", forRemoval = true)
public  class CategoryPageData extends AbstractPageData 

{


	
	public CategoryPageData()
	{
		// default constructor
	}
	

}