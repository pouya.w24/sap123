/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:04
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercewebservices.core.populator.options;

public enum PaymentInfoOption
{

	/** <i>Generated enum value</i> for <code>PaymentInfoOption.BASIC</code> value defined at extension <code>commercewebservices</code>. */
	BASIC , 

	/** <i>Generated enum value</i> for <code>PaymentInfoOption.BILLING_ADDRESS</code> value defined at extension <code>commercewebservices</code>. */
	BILLING_ADDRESS  


}
