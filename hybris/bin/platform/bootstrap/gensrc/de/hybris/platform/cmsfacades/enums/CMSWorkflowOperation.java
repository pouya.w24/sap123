/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:34:59
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsfacades.enums;

public enum CMSWorkflowOperation
{

	/** <i>Generated enum value</i> for <code>CMSWorkflowOperation.CANCEL_WORKFLOW</code> value defined at extension <code>cmsfacades</code>. */
	CANCEL_WORKFLOW , 

	/** <i>Generated enum value</i> for <code>CMSWorkflowOperation.MAKE_DECISION</code> value defined at extension <code>cmsfacades</code>. */
	MAKE_DECISION  


}
