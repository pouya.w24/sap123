/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:00
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercewebservicescommons.dto.product;

public enum PriceWsDTOType
{

	/** <i>Generated enum value</i> for <code>PriceWsDTOType.BUY</code> value defined at extension <code>commercewebservicescommons</code>. */
	BUY , 

	/** <i>Generated enum value</i> for <code>PriceWsDTOType.FROM</code> value defined at extension <code>commercewebservicescommons</code>. */
	FROM  


}
