/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:03
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsfacades.data;

import de.hybris.platform.cmsfacades.data.SyncRequestData;


import java.util.Objects;
/**
 * @deprecated no longer needed
 */
@Deprecated(since = "1811", forRemoval = true)
public  class SyncJobRequestData extends SyncRequestData 

{


	
	public SyncJobRequestData()
	{
		// default constructor
	}
	

}