/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:02
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.searchprovidercssearchservices.search.data;

import de.hybris.platform.searchprovidercssearchservices.search.data.AbstractExpressionAndValueQueryDTO;


import java.util.Objects;
public  class LessThanQueryDTO extends AbstractExpressionAndValueQueryDTO 

{


	
	public LessThanQueryDTO()
	{
		// default constructor
	}
	

}