/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:04
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmswebservices.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import java.util.Objects;
/**
 * Specifies properties of the product page.
 *
 * @deprecated no longer needed
 */
@ApiModel(value="ProductPageData", description="Specifies properties of the product page.")
@Deprecated(since = "6.6", forRemoval = true)
public  class ProductPageData extends AbstractPageData 

{


	
	public ProductPageData()
	{
		// default constructor
	}
	

}