/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:34:59
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.adaptivesearch.data;

import de.hybris.platform.adaptivesearch.data.AbstractAsItemConfiguration;


import java.util.Objects;
public abstract  class AbstractAsFacetValueConfiguration extends AbstractAsItemConfiguration 

{



	/** <i>Generated property</i> for <code>AbstractAsFacetValueConfiguration.value</code> property defined at extension <code>adaptivesearch</code>. */
	
	private String value;
	
	public AbstractAsFacetValueConfiguration()
	{
		// default constructor
	}
	
	public void setValue(final String value)
	{
		this.value = value;
	}

	public String getValue() 
	{
		return value;
	}
	

}