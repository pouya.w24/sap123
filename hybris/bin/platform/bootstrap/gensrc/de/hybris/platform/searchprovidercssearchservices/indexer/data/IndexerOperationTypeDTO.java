/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:00
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.searchprovidercssearchservices.indexer.data;

public enum IndexerOperationTypeDTO
{

	/** <i>Generated enum value</i> for <code>IndexerOperationTypeDTO.FULL</code> value defined at extension <code>searchprovidercssearchservices</code>. */
	FULL , 

	/** <i>Generated enum value</i> for <code>IndexerOperationTypeDTO.INCREMENTAL</code> value defined at extension <code>searchprovidercssearchservices</code>. */
	INCREMENTAL  


}
