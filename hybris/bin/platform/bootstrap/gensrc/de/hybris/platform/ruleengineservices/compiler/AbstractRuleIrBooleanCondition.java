/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:34:59
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ruleengineservices.compiler;

import de.hybris.platform.ruleengineservices.compiler.RuleIrCondition;


import java.util.Objects;
public abstract  class AbstractRuleIrBooleanCondition extends RuleIrCondition 

{


	
	public AbstractRuleIrBooleanCondition()
	{
		// default constructor
	}
	

}