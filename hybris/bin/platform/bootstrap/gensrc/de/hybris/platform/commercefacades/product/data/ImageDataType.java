/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:02
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercefacades.product.data;

public enum ImageDataType
{

	/** <i>Generated enum value</i> for <code>ImageDataType.PRIMARY</code> value defined at extension <code>commercefacades</code>. */
	PRIMARY , 

	/** <i>Generated enum value</i> for <code>ImageDataType.GALLERY</code> value defined at extension <code>commercefacades</code>. */
	GALLERY  


}
