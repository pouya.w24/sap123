/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:02
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.couponwebservices.dto.ws;

import de.hybris.platform.couponwebservices.dto.MultiCodeCouponWsDTO;
import de.hybris.platform.webservicescommons.dto.SearchPageWsDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;


import java.util.Objects;
/**
 * Multi code coupons search page
 */
@ApiModel(value="multiCodeCouponsSearchPage", description="Multi code coupons search page")
public  class MultiCodeCouponsSearchPageWsDTO extends SearchPageWsDTO<MultiCodeCouponWsDTO> 

{


	
	public MultiCodeCouponsSearchPageWsDTO()
	{
		// default constructor
	}
	

}