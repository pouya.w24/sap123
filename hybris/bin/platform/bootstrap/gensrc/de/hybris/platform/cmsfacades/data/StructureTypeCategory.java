/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:01
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmsfacades.data;

public enum StructureTypeCategory
{

	/** <i>Generated enum value</i> for <code>StructureTypeCategory.COMPONENT</code> value defined at extension <code>cmsfacades</code>. */
	COMPONENT , 

	/** <i>Generated enum value</i> for <code>StructureTypeCategory.PREVIEW</code> value defined at extension <code>cmsfacades</code>. */
	PREVIEW , 

	/** <i>Generated enum value</i> for <code>StructureTypeCategory.PAGE</code> value defined at extension <code>cmsfacades</code>. */
	PAGE , 

	/** <i>Generated enum value</i> for <code>StructureTypeCategory.RESTRICTION</code> value defined at extension <code>cmsfacades</code>. */
	RESTRICTION  


}
