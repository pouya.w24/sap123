/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:03
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.personalizationfacades.enums;

public enum ItemStatus
{

	/** <i>Generated enum value</i> for <code>ItemStatus.ENABLED</code> value defined at extension <code>personalizationfacades</code>. */
	ENABLED , 

	/** <i>Generated enum value</i> for <code>ItemStatus.DISABLED</code> value defined at extension <code>personalizationfacades</code>. */
	DISABLED , 

	/** <i>Generated enum value</i> for <code>ItemStatus.DELETED</code> value defined at extension <code>personalizationfacades</code>. */
	DELETED  


}
