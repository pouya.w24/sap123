/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:01
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.adaptivesearchbackoffice.data;

import de.hybris.platform.adaptivesearchbackoffice.data.AbstractSortConfigurationEditorData;


import java.util.Objects;
public  class PromotedSortEditorData extends AbstractSortConfigurationEditorData 

{


	
	public PromotedSortEditorData()
	{
		// default constructor
	}
	

}