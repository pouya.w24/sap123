/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:03
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.b2bacceleratorfacades.order.data;


import java.util.Objects;
/**
 * @deprecated Use {@link de.hybris.platform.b2bapprovalprocessfacades.company.data.B2BPermissionTypeData} instead.
 */
@Deprecated(since = "6.0", forRemoval = true)
public  class B2BPermissionTypeData extends de.hybris.platform.b2bapprovalprocessfacades.company.data.B2BPermissionTypeData 

{


	
	public B2BPermissionTypeData()
	{
		// default constructor
	}
	

}