/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:34:59
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.commercewebservicescommons.dto.product;

public enum ReturnRequestStatusWsDTOType
{

	/** <i>Generated enum value</i> for <code>ReturnRequestStatusWsDTOType.CANCELLING</code> value defined at extension <code>commercewebservicescommons</code>. */
	CANCELLING  


}
