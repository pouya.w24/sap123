/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:34:59
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.cmscockpit.session.script;

public enum ScriptGroup
{

	/** <i>Generated enum value</i> for <code>ScriptGroup.LIVEEDIT,LIVEEDIT_SCRIPT_FILE</code> value defined at extension <code>cmscockpit</code>. */
	LIVEEDIT,LIVEEDIT_SCRIPT_FILE  


}
