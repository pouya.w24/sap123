/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:04
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ruleengineservices.rao.providers;

public enum RAOProviderOption
{

	/** <i>Generated enum value</i> for <code>RAOProviderOption.EXPAND_COUPONS</code> value defined at extension <code>couponservices</code>. */
	EXPAND_COUPONS  


}
