/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:03
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.ruleengine;

public enum InitializeMode
{

	/** <i>Generated enum value</i> for <code>InitializeMode.RESTORE</code> value defined at extension <code>ruleengine</code>. */
	RESTORE , 

	/** <i>Generated enum value</i> for <code>InitializeMode.NEW</code> value defined at extension <code>ruleengine</code>. */
	NEW  


}
