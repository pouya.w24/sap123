/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:03
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.sap.productconfig.runtime.interf.model;

public enum ProductConfigMessagePromoType
{

	/** <i>Generated enum value</i> for <code>ProductConfigMessagePromoType.PROMO_APPLIED</code> value defined at extension <code>sapproductconfigruntimeinterface</code>. */
	PROMO_APPLIED , 

	/** <i>Generated enum value</i> for <code>ProductConfigMessagePromoType.PROMO_OPPORTUNITY</code> value defined at extension <code>sapproductconfigruntimeinterface</code>. */
	PROMO_OPPORTUNITY  


}
