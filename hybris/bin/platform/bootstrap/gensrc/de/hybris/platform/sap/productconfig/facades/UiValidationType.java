/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:03
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.sap.productconfig.facades;

public enum UiValidationType
{

	/** <i>Generated enum value</i> for <code>UiValidationType.NUMERIC</code> value defined at extension <code>sapproductconfigfacades</code>. */
	NUMERIC , 

	/** <i>Generated enum value</i> for <code>UiValidationType.NONE</code> value defined at extension <code>sapproductconfigfacades</code>. */
	NONE  


}
