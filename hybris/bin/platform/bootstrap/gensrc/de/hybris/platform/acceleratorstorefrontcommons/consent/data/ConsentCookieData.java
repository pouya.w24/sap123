/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:34:58
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.acceleratorstorefrontcommons.consent.data;

import de.hybris.platform.commercefacades.consent.data.AnonymousConsentData;


import java.util.Objects;
/**
 * @deprecated Use {@link AnonymousConsentData} instead
 */
@Deprecated(since = "1905", forRemoval = true)
public  class ConsentCookieData extends AnonymousConsentData 

{


	
	public ConsentCookieData()
	{
		// default constructor
	}
	

}