/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:04
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.b2bacceleratorfacades.order.data;

public enum B2BReplenishmentRecurrenceEnum
{

	/** <i>Generated enum value</i> for <code>B2BReplenishmentRecurrenceEnum.DAILY</code> value defined at extension <code>b2bacceleratorfacades</code>. */
	DAILY , 

	/** <i>Generated enum value</i> for <code>B2BReplenishmentRecurrenceEnum.WEEKLY</code> value defined at extension <code>b2bacceleratorfacades</code>. */
	WEEKLY , 

	/** <i>Generated enum value</i> for <code>B2BReplenishmentRecurrenceEnum.MONTHLY</code> value defined at extension <code>b2bacceleratorfacades</code>. */
	MONTHLY  


}
