/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:02
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.sap.productconfig.frontend.util.impl;

public enum ErrorType
{

	/** <i>Generated enum value</i> for <code>ErrorType.ERROR</code> value defined at extension <code>ysapproductconfigaddon</code>. */
	ERROR , 

	/** <i>Generated enum value</i> for <code>ErrorType.MANDATORY_FIELD</code> value defined at extension <code>ysapproductconfigaddon</code>. */
	MANDATORY_FIELD , 

	/** <i>Generated enum value</i> for <code>ErrorType.CONFLICT</code> value defined at extension <code>ysapproductconfigaddon</code>. */
	CONFLICT  


}
