/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:04
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.customercouponfacades.emums;

public enum AssignCouponResult
{

	/** <i>Generated enum value</i> for <code>AssignCouponResult.SUCCESS</code> value defined at extension <code>customercouponfacades</code>. */
	SUCCESS , 

	/** <i>Generated enum value</i> for <code>AssignCouponResult.ASSIGNED</code> value defined at extension <code>customercouponfacades</code>. */
	ASSIGNED , 

	/** <i>Generated enum value</i> for <code>AssignCouponResult.INEXISTENCE</code> value defined at extension <code>customercouponfacades</code>. */
	INEXISTENCE  


}
