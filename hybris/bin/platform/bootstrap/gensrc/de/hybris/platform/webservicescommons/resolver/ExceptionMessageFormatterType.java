/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:00
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.webservicescommons.resolver;

public enum ExceptionMessageFormatterType
{

	/** <i>Generated enum value</i> for <code>ExceptionMessageFormatterType.FORWARD</code> value defined at extension <code>webservicescommons</code>. */
	FORWARD , 

	/** <i>Generated enum value</i> for <code>ExceptionMessageFormatterType.GENERIC</code> value defined at extension <code>webservicescommons</code>. */
	GENERIC  


}
