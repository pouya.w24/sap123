/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:35:03
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.solrfacetsearch.config;

public enum FacetType
{

	/** <i>Generated enum value</i> for <code>FacetType.Refine</code> value defined at extension <code>solrfacetsearch</code>. */
	REFINE , 

	/** <i>Generated enum value</i> for <code>FacetType.MultiSelectAnd</code> value defined at extension <code>solrfacetsearch</code>. */
	MULTISELECTAND , 

	/** <i>Generated enum value</i> for <code>FacetType.MultiSelectOr</code> value defined at extension <code>solrfacetsearch</code>. */
	MULTISELECTOR  


}
