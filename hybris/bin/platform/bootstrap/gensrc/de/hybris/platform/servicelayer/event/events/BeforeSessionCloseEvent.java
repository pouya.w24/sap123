/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN!
 * --- Generated at 21.10.2022, 11:34:59
 * ----------------------------------------------------------------
 *
 * Copyright (c) 2022 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.platform.servicelayer.event.events;

import java.io.Serializable;
import de.hybris.platform.servicelayer.event.events.AbstractEvent;

public  class BeforeSessionCloseEvent  extends AbstractEvent 
{

	
	public BeforeSessionCloseEvent()
	{
		super();
	}

	public BeforeSessionCloseEvent(final Serializable source)
	{
		super(source);
	}
	


}
