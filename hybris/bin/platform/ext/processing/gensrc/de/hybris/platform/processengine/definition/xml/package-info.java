//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.11 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2022.10.21 um 11:35:15 AM CEST 
//

@javax.xml.bind.annotation.XmlSchema(namespace = "http://www.hybris.de/xsd/processdefinition", elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
package de.hybris.platform.processengine.definition.xml;
