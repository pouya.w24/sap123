const TRACKING_URL = "https://license.hybris.com/collect";

var _paq = _paq || [];

_paq.push(["enableLinkTracking"]);

function embedTrackingCode() {
    _paq.push(["setTrackerUrl", TRACKING_URL]);
    _paq.push(["setSiteId", siteId]);

    loadPiwikLib();
    trackingExplorerTreeNavigation();
    trackingPerspectiveNavigation();
    trackingActionButtonNavigation();
}

function trackingExplorerTreeNavigation() {
    $(document).on('click', '.yw-navigationNode-level1', function () {
        if (isInNavigationContainer($(this))) {
            _paq.push(['trackEvent', 'Backoffice', 'ExplorerTree_Navigation_Level_1', $(this).attr("title")]);
        }
    });

    $(document).on('click', '.yw-navigationNode-level2', function () {
        if (isInNavigationContainer($(this))) {
            _paq.push(['trackEvent', 'Backoffice', 'ExplorerTree_Navigation_Level_2', $(this).prevAll('tr.yw-navigationNode-level1').attr("title") + ' ► ' + $(this).attr("title")]);
        }
    });

    $(document).on('click', '.yw-navigationNode-level3', function () {
        if (isInNavigationContainer($(this))) {
            _paq.push(['trackEvent', 'Backoffice', 'ExplorerTree_Navigation_Level_3', $(this).prevAll('tr.yw-navigationNode-level1').attr("title") + ' ► ' + $(this).prevAll('tr.yw-navigationNode-level2').attr("title") + ' ► ' + $(this).attr("title")]);
        }
    });
}

function trackingPerspectiveNavigation() {
    $(document).on('click', '.yw-perspective-popup-level0', function () {
        _paq.push(['trackEvent', 'Backoffice', 'Perspective_Navigation', $(this).attr("title")]);
    });
}

function trackingActionButtonNavigation() {
    $(document).on('click', '.cng-action-enabled', function () {
        if (isNavigationAction($(this))) {
            _paq.push(['trackEvent', 'Backoffice', 'ActionButton_Navigation', getLabelTitle($(this))]);
        }
    });
}

function isInNavigationContainer(element) {
    return element.parents() != null && element.parents(".z-west-body").length > 0
}

function isNavigationAction(element) {
    return element.parents() != null && element.parents(".yw-pcmbackoffice-navibar").length > 0
}

function getLabelTitle(element) {
    var labels = element.find("span");
    return labels.length > 0 ? labels[0].innerHTML : "unknown";
}

function loadPiwikLib() {
    var piwikScript = document.createElement("script");
    piwikScript.type = "text/javascript";
    piwikScript.src = "cng/scripts/piwik.js";
    document.head.appendChild(piwikScript);
}
